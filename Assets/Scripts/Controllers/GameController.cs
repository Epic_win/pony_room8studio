﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] private InputController _inputController;
    [SerializeField] private Pool _pool;
    [SerializeField] private TargetZoneController _targetZone;
    [SerializeField] private BonusController _bonusController;
    [SerializeField] private Gui _gui;

    public static GameController Instance;

    public Vector3 MinScreenPosition { get; private set; }
    public Vector3 MaxScreenPosition { get; private set; }

    public Pool Pool { get { return _pool; } }

    public event Action<Vector3> FieldTap = delegate { };
    public event Action<ControlledAnimal> AnimalTap = delegate { };
    public event Action<IFollowing, int> AnimalReachedTarget = delegate { };
    public event Action<LevelDataInput> StartLevel = delegate { };
    public event Action<LevelDataOutput> EndLevel = delegate { };

    private LevelDataInput _currentLevel;
    private int _secondPassed;
    private IEnumerator _timer;
    

    void Awake()
    {
        Instance = this;

        _inputController.FieldTap += OnFieldTap;
        _inputController.AnimalTap += OnAnimalTap;

        _targetZone.TargetReached += OnTargetReached;

        _gui.MessageViewPlayPressed += OnButtonPlayPressed;
        _gui.MessageViewRetryPressed += Play;

        _bonusController.BonusCollected += OnBonusCollected;

        MinScreenPosition = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        MaxScreenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }

    private void OnBonusCollected(int secondReward)
    {
        _currentLevel.BonusTime += secondReward;
    }

    private void OnAnimalTap(ControlledAnimal controlledAnimal)
    {
        AnimalTap(controlledAnimal);
    }

    private void OnFieldTap(Vector3 vector3)
    {
        FieldTap(vector3);
    }

    private void IncreaseLevel()
    {
        if (_currentLevel.Level == 0)
            _currentLevel.SetDefault();
        else
            _currentLevel.Next();
    }

    private void OnButtonPlayPressed()
    {
        IncreaseLevel();
        Play();
    }

    private void Play()
    {
        _timer = null;
        _secondPassed = 0;
        StartLevel(_currentLevel);
        _timer = Timer();
        StartCoroutine(_timer);

        _gui.OpenLast();
    }

    private IEnumerator Timer()
    {
        while (_timer != null)
        {
            _gui.UpdateGameTimer(_secondPassed, (_currentLevel.SecondsToPass + _currentLevel.BonusTime));
            yield return new WaitForSeconds(1);
            _secondPassed++;
        }
    }

    private void OnTargetReached(IFollowing following, int totalAnimals)
    {
        AnimalReachedTarget(following, totalAnimals);

        if (_currentLevel.FollowingAnimalsCount == totalAnimals)
        {
            StopAllCoroutines();
            LevelDataOutput data;
            data.Level = _currentLevel.Level;
            data.DeltaSeconds = _secondPassed - (_currentLevel.SecondsToPass + _currentLevel.BonusTime);
            EndLevel(data);
        }
    }

    public Vector3 RandomPosition()
    {
        return new Vector3(
            Random.Range(MinScreenPosition.x, MaxScreenPosition.x),
            Random.Range(MinScreenPosition.y, MaxScreenPosition.y), 0);
    }
}

public struct LevelDataInput
{
    public int Level;
    public int SecondsToPass;
    public int FollowingAnimalsCount;
    public int ControlledAnimalsCount;
    public int BonusTime;

    public void SetDefault()
    {
        Level = 1;
        SecondsToPass = 30;
        ControlledAnimalsCount = 2;
        FollowingAnimalsCount = 4;
        BonusTime = 0;
    }

    public void Next()
    {
        Level++;
        SecondsToPass += 5;
        ControlledAnimalsCount += (Level%5 == 0 ? 1 : 0);
        FollowingAnimalsCount += 4;
        BonusTime = 0;
    }
}

public struct LevelDataOutput
{
    public int Level;
    public int DeltaSeconds;
    public bool Passed
    {
        get { return DeltaSeconds <= 0; }
    }
}