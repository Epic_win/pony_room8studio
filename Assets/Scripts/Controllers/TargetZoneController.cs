﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetZoneController : MonoBehaviour
{
    private List<IFollowing> _animals = new List<IFollowing>();

    public event Action<IFollowing, int> TargetReached = delegate { };

    void Start()
    {
        GameController.Instance.EndLevel += OnEndLevel;
    }

    private void OnEndLevel(LevelDataOutput output)
    {
        _animals.Clear();
    }

    public bool ExistAnimal(IFollowing animal)
    {
        return _animals.Contains(animal);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var animal = other.GetComponent<FollowingAnimal>();
        if (animal != null && !_animals.Contains(animal))
        {
            _animals.Add(animal);
            TargetReached(animal, _animals.Count);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        var animal = other.GetComponent<FollowingAnimal>();
        if (animal != null && _animals.Contains(animal))
            _animals.Remove(animal);
    }
}
