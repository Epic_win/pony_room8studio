﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public event Action<Vector3> FieldTap = delegate{  };
    public event Action<ControlledAnimal> AnimalTap = delegate{  };

    void Start()
    {
        GameController.Instance.StartLevel += OnStartLevel;
        GameController.Instance.EndLevel += OnEndLevel;
    }

    private void OnEndLevel(LevelDataOutput obj)
    {
        StopAllCoroutines();
    }

    private void OnStartLevel(LevelDataInput dataInput)
    {
        StartCoroutine(Readout());
    }

    IEnumerator Readout()
    {
        while (gameObject.activeInHierarchy)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition) - Camera.main.transform.position;
                RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
  
                if (hit)
                {
                    var obj = hit.collider.gameObject.GetComponent<ControlledAnimal>();
                    if (obj != null)
                        AnimalTap(obj);
                    else
                        FieldTap(mousePos);
                }
                else
                    FieldTap(mousePos);
            }

            yield return null;
        }
    }
}
