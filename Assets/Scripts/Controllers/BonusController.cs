﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BonusController : MonoBehaviour
{
    private int _animalsCointInPaddock;
    private const int _bonusTrigger = 5;

    private List<IBonus> _bonuses = new List<IBonus>();

    public event Action<int> BonusCollected = delegate { };

    void Start()
    {
        GameController.Instance.AnimalReachedTarget += OnAnimalReachedTarget;
        GameController.Instance.EndLevel += OnEndLevel;
    }

    private void OnEndLevel(LevelDataOutput levelDataOutput)
    {
        var bonuses = _bonuses;
        for (int i = 0; i < bonuses.Count; i++)
            RemoveBonus(bonuses[i]);
        _bonuses.Clear();
        _animalsCointInPaddock = 0;
    }

    private void OnAnimalReachedTarget(IFollowing following, int totalAnimals)
    {
        _animalsCointInPaddock++;
        if (_animalsCointInPaddock%_bonusTrigger == 0)
            AddBonus();
    }

    private void AddBonus()
    {
        var bonus = (IBonus)GameController.Instance.Pool.Get(ObjectType.BonusCircle);
        bonus.LifeTimeHasPassed += OnLifeTimeHasPassed;
        bonus.Collected += OnCollected;
        _bonuses.Add(bonus);
    }

    private void OnCollected(IBonus bonus)
    {
        BonusCollected(bonus.SecondsReward);
        RemoveBonus(bonus);
    }

    private void OnLifeTimeHasPassed(IBonus bonus)
    {
        RemoveBonus(bonus);
    }

    private void RemoveBonus(IBonus bonus)
    {
        bonus.LifeTimeHasPassed -= OnLifeTimeHasPassed;
        bonus.Collected -= OnCollected;
        _bonuses.Remove(bonus);
        GameController.Instance.Pool.Put((IPoolable)bonus);
    }

}
