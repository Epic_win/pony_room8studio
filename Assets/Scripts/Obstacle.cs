﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.Animals;

public class Obstacle : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        var animal = other.GetComponent<Animal>();
        if (animal != null)
            animal.OnObstacle();
    }
}
