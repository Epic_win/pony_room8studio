﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Views
{
    public class GameView : View
    {
        [SerializeField] private Image _timer;
        [SerializeField] private Text _timerTime;
        [SerializeField] private Image _cursor;

        private readonly Color _visible = new Color(1, 1, 1, 0.5f);

        void Start()
        {
            GameController.Instance.EndLevel += OnEndLevel;
            GameController.Instance.FieldTap += OnFieldTap;
        }

        private void OnFieldTap(Vector3 vector3)
        {
            _cursor.color = _visible;
            var position = Camera.main.WorldToScreenPoint(vector3);
            _cursor.gameObject.transform.position = position;
        }

        private void OnEndLevel(LevelDataOutput output)
        {
            _timer.fillAmount = 1;
            _cursor.color = Color.clear;
        }

        public void UpdateTimer(float passedSeconds, float totalSeconds)
        {
            _timer.fillAmount = 1 - passedSeconds/totalSeconds;
            var deltaTime = totalSeconds - passedSeconds;
            if (deltaTime >= 0)
                _timerTime.text = deltaTime.ToString();
        }

    }
}
