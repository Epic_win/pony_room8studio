﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MessageView : View
{
    [SerializeField] private Text _description;
    [SerializeField] private Button _buttonPlay;
    [SerializeField] private Button _buttonRetry;

    public event Action ButtonPlayPressed = delegate { };
    public event Action ButtonRetryPressed = delegate { };

    void Start()
    {
        _buttonPlay.onClick.AddListener(OnButtonPlay);
        _buttonRetry.onClick.AddListener(OnButtonRetry);
        GameController.Instance.EndLevel += OnEndLevel;
    }

    private void OnButtonRetry()
    {
        ButtonRetryPressed();
    }

    private void OnEndLevel(LevelDataOutput output)
    {
        _buttonPlay.interactable = output.Passed;
        Show(output.Passed ?
            "LEVEL №" + output.Level + " PASSED" :
            "LEVEL №" + output.Level + " FAILED \n" +
            "(late for " + output.DeltaSeconds + " seconds)");
    }

    private void OnButtonPlay()
    {
        ButtonPlayPressed();
    }

    public void Show(string description, bool retryActive = true)
    {
        base.Show();
        _description.text = description;
        _buttonRetry.gameObject.SetActive(retryActive);
    }
}
