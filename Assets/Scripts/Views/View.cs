﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class View : MonoBehaviour
{
    public bool IsOpen { get; private set; }

    public event Action<View> Opened = delegate {  };

    void Awake()
    {
        IsOpen = gameObject.activeInHierarchy;
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
        IsOpen = true;
        Opened(this);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
        IsOpen = false;
    }
}
