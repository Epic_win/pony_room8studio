﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Views;

public class Gui : MonoBehaviour
{
    [SerializeField] MessageView _messageView;
    [SerializeField] GameView _gameView;

    private View _previousView;
    private View _currentView;

    public event Action MessageViewPlayPressed = delegate { };
    public event Action MessageViewRetryPressed = delegate { };

    void Awake()
    {
        _messageView.Opened += OnOpened;
        _gameView.Opened += OnOpened;

        _currentView = _gameView;
        _messageView.Show("WELCOME!", false);

        _messageView.ButtonPlayPressed += () => MessageViewPlayPressed();
        _messageView.ButtonRetryPressed += () => MessageViewRetryPressed();
    }

    private void OnOpened(View view)
    {
        if (_currentView != null && _currentView != view)
        {
            _currentView.Hide();
            _previousView = _currentView;
        }

        _currentView = view;
    }

    public void OpenLast()
    {
        if (_previousView == null) return;

        if (_currentView != null)
            _currentView.Hide();

        _currentView = _previousView;
        _currentView.Show();
        _previousView = null;
    }

    public void UpdateGameTimer(float passedSeconds, float totalSeconds)
    {
        _gameView.UpdateTimer(passedSeconds, totalSeconds);
    }
}
