﻿using UnityEngine;
using System.Collections;

public interface IFollowing
{
    float Offset { get; }
    bool IsGoalReached { set; get; }
}
