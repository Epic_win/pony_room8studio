﻿public interface IPoolable
{
    ObjectType ObjectType { get; }

    void OnPutToPull();
    void OnGetFromPull();
}
