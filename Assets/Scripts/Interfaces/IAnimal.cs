﻿using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IAnimal
    {
        Rect SpawnScreenPart { get; }
        float Speed { get; }
        void Move(Vector3 target);
        void OnObstacle();
    }
}

