﻿using UnityEngine;
using System.Collections;

public interface IControllable
{
    bool Selected { set; get; }
    float Radius { get; }
}
