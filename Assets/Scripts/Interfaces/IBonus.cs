﻿using System;
using UnityEngine;
using System.Collections;

public interface IBonus
{
    event Action<IBonus> LifeTimeHasPassed;
    event Action<IBonus> Collected;

    int SecondsReward { get; }
    float LifeTime { get; }

    //void SetOnField();
}
