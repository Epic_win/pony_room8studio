﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Animals;

public class Pool: MonoBehaviour
{
    [SerializeField] private List<PoolData> _animalsPrefabs;

    private List<IPoolable> _containerList = new List<IPoolable>();

    public void Put(IPoolable obj)
    {
        obj.OnPutToPull();
        _containerList.Add(obj);
    }

    public IPoolable Get(ObjectType type)
    {
        IPoolable obj = null;

        for (int i = 0; i < _containerList.Count; i++)
        {
            if (_containerList[i].ObjectType != type) continue;

            obj = _containerList[i];
            _containerList.RemoveAt(i);
            break;
        }

        if (obj == null)
        {
            var prefab = _animalsPrefabs.Where(data => data.Type == type).Select(data => data.Prefab).First();
            obj = Instantiate(prefab).GetComponent<IPoolable>();
        }

        obj.OnGetFromPull();
        return obj;
    }
}

public enum ObjectType
{
    Pony,
    Dog,
    BonusCircle
}

[Serializable]
public struct PoolData
{
    public ObjectType Type;
    public GameObject Prefab;
}