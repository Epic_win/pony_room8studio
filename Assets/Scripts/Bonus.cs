﻿using System;
using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour, IPoolable, IBonus
{
    public int SecondsReward { get { return 5; } }
    public float LifeTime { get { return 10; } }
    public ObjectType ObjectType { get {return ObjectType.BonusCircle; } }

    public event Action<IBonus> LifeTimeHasPassed = delegate { };
    public event Action<IBonus> Collected = delegate { };

    private IEnumerator Timer()
    {
        yield return new WaitForSeconds(LifeTime);
        LifeTimeHasPassed(this);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        var animal = other.GetComponent<ControlledAnimal>();
        if (animal != null)
        {
            StopAllCoroutines();
            Collected(this);
        }
    }

    public void OnPutToPull()
    {
        StopAllCoroutines();
        gameObject.SetActive(false);
    }

    public void OnGetFromPull()
    {
        gameObject.SetActive(true);
        transform.position = GameController.Instance.RandomPosition();
        StartCoroutine(Timer());
    }

}
