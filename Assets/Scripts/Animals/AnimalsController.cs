﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Interfaces;

public class AnimalsController : MonoBehaviour
{
    private List<IAnimal> _animals = new List<IAnimal>();
    private List<FollowingAnimal> _controlledGroup = new List<FollowingAnimal>();

    private IEnumerator _detecting;

    private ControlledAnimal _selectedAnimal;
    private ControlledAnimal SelectedAnimal
    {
        get { return _selectedAnimal; }
        set
        {
            if (_selectedAnimal != null)
                _selectedAnimal.Selected = false;

            _controlledGroup.Clear();
            _selectedAnimal = value;
            if (_selectedAnimal == null) return;

            _selectedAnimal.Selected = true;
            if (_detecting == null)
            {
                _detecting = RangeDetection();
                StartCoroutine(_detecting);
            }
        }
    }

    void Start()
    {
        GameController.Instance.StartLevel += Initialize;
        GameController.Instance.FieldTap += OnFieldTap;
        GameController.Instance.AnimalTap += OnAnimalTap;
        GameController.Instance.AnimalReachedTarget += OnTargetReached;
        GameController.Instance.EndLevel += Reset;
    }


    private void OnTargetReached(IFollowing following, int animalsCount)
    {
        following.IsGoalReached = true;
    }

    private void OnAnimalTap(ControlledAnimal animal)
    {
        SelectedAnimal = animal;
    }

    private void OnFieldTap(Vector3 tapPosition)
    {
        if (SelectedAnimal == null) return;

        ((IAnimal)SelectedAnimal).Move(tapPosition);

        for (int i = 0; i < _controlledGroup.Count; i++)
            if (!_controlledGroup[i].IsGoalReached)
                _controlledGroup[i].Move(tapPosition);
    }

    private void Initialize(LevelDataInput dataInput)
    {
        for (int i = 0; i < dataInput.FollowingAnimalsCount; i++)
            AddAnimal((IAnimal)GameController.Instance.Pool.Get(ObjectType.Pony));

        for (int i = 0; i < dataInput.ControlledAnimalsCount; i++)
            AddAnimal((IAnimal)GameController.Instance.Pool.Get(ObjectType.Dog));
    }

    private void AddAnimal(IAnimal animal)
    {
        _animals.Add(animal);
    }

    IEnumerator RangeDetection()
    {
        while (SelectedAnimal != null)
        {
            var hit = Physics2D.CircleCastAll(SelectedAnimal.transform.position, SelectedAnimal.Radius, Vector2.zero);
            if (hit.Length > 0)
            {
                for (int i = 0; i < hit.Length; i++)
                {
                    var obj = hit[i].collider.gameObject.GetComponent<FollowingAnimal>();
                    if (obj != null)
                    {
                        if (obj.IsGoalReached)
                            _controlledGroup.Remove(obj);
                        else
                            _controlledGroup.Add(obj);
                    }
                }
            }
            yield return null;
        }
        _detecting = null;
    }


    private void Reset(LevelDataOutput output)
    {
        SelectedAnimal = null;
        for (int i = 0; i < _animals.Count; i++)
            GameController.Instance.Pool.Put((IPoolable)_animals[i]);

        _controlledGroup.Clear();
        _animals.Clear();
    }
}
