﻿using System;
using UnityEngine;
using System.Collections;

public class AnimalAnimation : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private State _state;
    private Sprite _defautSprite;

    public SpriteRenderer SpriteRenderer { get { return _spriteRenderer; } }

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _defautSprite = _spriteRenderer.sprite;
    }

    public void Play(State state, bool play)
    {
        _animator.SetBool(_state.ToString(), false);
        _state = state;
        _animator.SetBool(state.ToString(), play);
    }

    public void Reset()
    {
        _animator.SetBool(_state.ToString(), false);
        _spriteRenderer.sprite = _defautSprite;
    }
}

public enum State
{
    Right,
    Left,
    Front,
    Back
}