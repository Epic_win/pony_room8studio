﻿using System;
using System.Collections;
using Assets.Scripts.Interfaces;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Assets.Scripts.Animals
{
    public class Animal : MonoBehaviour, IPoolable, IAnimal
    {
        public virtual ObjectType ObjectType { get; private set; }
        public virtual float Speed { get; private set; }
        public virtual Rect SpawnScreenPart { get; private set; }

        protected Vector3 MinPosition;
        protected Vector3 MaxPosition;
        protected BoxCollider2D Collider;
        protected AnimalAnimation Animation;

        protected virtual Vector3 TargetPosition { get; set; }

        private IEnumerator _following;

        protected const float MovementThreshold = 0.01f;

        protected virtual void Awake()
        {
            Collider = GetComponent<BoxCollider2D>();
            Animation = GetComponent<AnimalAnimation>();
            MinPosition = GameController.Instance.MinScreenPosition + (Vector3)Collider.size / 2;
            MaxPosition = GameController.Instance.MaxScreenPosition - (Vector3)Collider.size / 2;
            transform.SetParent(GameController.Instance.Pool.transform);
        }

        public virtual void Move(Vector3 target)
        {
            TargetPosition = target;
            PlayAnimation(true);

            if (_following == null)
            {
                _following = Following();
                StartCoroutine(_following);
            }
        }

        public virtual void OnObstacle()
        {
        }

        protected virtual IEnumerator Following()
        {
            while (_following != null)
            {
                transform.position = Vector3.MoveTowards(transform.position, TargetPosition, Time.deltaTime * Speed);
                yield return null;

                if ((TargetPosition - transform.position).magnitude < MovementThreshold)
                    break;
            }

            PlayAnimation(false);
            _following = null;
        }

        protected virtual void PlayAnimation(bool walk)
        {
            var difference = TargetPosition - transform.position;

            if (Math.Abs(difference.x) > Math.Abs(difference.y))
                Animation.Play(TargetPosition.x > transform.position.x ? State.Right : State.Left, walk);
            else
                Animation.Play(TargetPosition.y > transform.position.y ? State.Back : State.Front, walk);
        }
    
        public virtual void OnPutToPull()
        {
            StopAllCoroutines();
            _following = null;
            Animation.Reset();
            gameObject.SetActive(false);
        }

        public virtual void OnGetFromPull()
        {
            gameObject.SetActive(true);

            var minPositionForSpawn = Camera.main.ScreenToWorldPoint(new Vector3(SpawnScreenPart.xMin, SpawnScreenPart.yMin, 0)) + (Vector3)Collider.size / 2;
            var maxPositionForSpawn = Camera.main.ScreenToWorldPoint(new Vector3(SpawnScreenPart.xMax, SpawnScreenPart.yMax, 0)) - (Vector3)Collider.size / 2;

            transform.position = new Vector3(
                Random.Range(minPositionForSpawn.x, maxPositionForSpawn.x),
                Random.Range(minPositionForSpawn.y, maxPositionForSpawn.y),
                transform.position.z);
        }
    }
}
