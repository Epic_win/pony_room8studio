﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Animals;
using Random = UnityEngine.Random;

public class ControlledAnimal : Animal, IControllable
{
    public virtual float Radius { get; private set; }

    private bool _selected;
    public bool Selected
    {
        get { return _selected; }
        set
        {
            _selected = value;
            Animation.SpriteRenderer.color = value ? Color.white : Color.gray;
        }
    }

    private Vector3 _targetPosition;
    protected override Vector3 TargetPosition
    {
        get { return _targetPosition; }
        set
        {
            _targetPosition = new Vector3(
                Mathf.Clamp(value.x, MinPosition.x, MaxPosition.x),
                Mathf.Clamp(value.y, MinPosition.y, MaxPosition.y),
                transform.position.z);
        }
    }
    
    public override void OnObstacle()
    {
        TargetPosition = transform.position - (TargetPosition - transform.position).normalized / 2;
    }

    public override void OnGetFromPull()
    {
        Selected = false;
        base.OnGetFromPull();
    }
}
