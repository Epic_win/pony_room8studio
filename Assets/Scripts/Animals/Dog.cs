﻿using UnityEngine;
using System.Collections;

public class Dog : ControlledAnimal
{
    public override ObjectType ObjectType { get { return ObjectType.Dog; } }
    public override float Radius { get { return 0.3f; } }
    public override float Speed { get { return 0.7f; } }
    public override Rect SpawnScreenPart
    {
        get
        {
            return Rect.MinMaxRect(
                Screen.width * 3 / 4f, Screen.height * 2 / 3f,
                Screen.width, Screen.height);
        }
    }
}
