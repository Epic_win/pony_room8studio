﻿using System;
using UnityEngine;
using System.Collections;
using Assets.Scripts.Animals;
using Random = UnityEngine.Random;

public class FollowingAnimal : Animal, IFollowing
{
    public float Offset { get; protected set; }

    public bool IsGoalReached { set; get; }

    private Vector3 _targetPosition;
    protected override Vector3 TargetPosition
    {
        get { return _targetPosition; }
        set
        {
            var newValue = value + (Vector3)(Vector2.one * Offset);
            _targetPosition = new Vector3(
                Mathf.Clamp(newValue.x, MinPosition.x, MaxPosition.x),
                Mathf.Clamp(newValue.y, MinPosition.y, MaxPosition.y),
                transform.position.z);
        }
    }


    public override void OnObstacle()
    {
        if (IsGoalReached) return;

        TargetPosition = transform.position;
    }

    public override void OnGetFromPull()
    {
        IsGoalReached = false;
        base.OnGetFromPull();
    }
}
