﻿using UnityEngine;
using System.Collections;

public class Pony : FollowingAnimal
{
    public override ObjectType ObjectType { get { return ObjectType.Pony; } }
    public override float Speed { get { return 0.7f; } }
    public override Rect SpawnScreenPart { get { return Rect.MinMaxRect(0, 0, Screen.width, Screen.height / 3f); } }

    private const float MinOffset = 0.05f;
    private const float MaxOffset = 0.2f;

    protected override void Awake()
    {
        base.Awake();
        Offset = Random.Range(MinOffset, MaxOffset) * (Random.Range(0, 2) == 1 ? 1 : -1);
    }
}
